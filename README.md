Module unicaen/idref
====================

Ce module fournit de quoi :

- Une aide de vue `IdrefPopupTriggerViewHelper` (invoquée via `$this->idrefPopupTrigger()`) dessinant un bouton 
  permettant d'ouvrir l'interface web IdRef de recherche/sélection d'une notice pour "rappatrier" l'identifiant 
  IdRef (PPN) associé dans un champ texte (ou une div) de votre appli. 
  Cf. https://documentation.abes.fr/aideidrefdeveloppeur/index.html#InterconnecterBaseEtIdref

  ![](doc/captureBoutonIdRef.png)


- Une aide de vue `IdrefLinkViewHelper` (invoquée via `$this->idrefLink()`) dessinant un identifiant IdRef (PPN) 
  sous la forme d'un lien pointant vers la page pérenne de la notice IdRef (www.idref.fr). 
  Ex : `<a href="https://www.idref.fr/175266549">175266549</a>`


Démo
====

Une fois le module installé, une page de démo est accessible à l'adresse `/unicaen-idref` de votre appli.


Installation
============

```bash
composer require unicaen/idref
```

**Une fois le module installé, le répertoire `public/unicaen` de ce module doit etre copié dans le répertoire "public/" 
de votre appli.**

Pour que ceci soit fait automatiquement à chaque `composer install`, ajoutez ceci aux "post-install commands" 
de votre `composer.json` :

```json
"scripts": {
     "post-install-cmd": [
         "mkdir -p public/unicaen && cp -r vendor/unicaen/idref/public/unicaen public/"
     ]
}
```
