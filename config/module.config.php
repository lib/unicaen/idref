<?php

namespace UnicaenIdref;

use UnicaenIdref\Controller\IndexController;
use UnicaenIdref\Controller\IndexControllerFactory;
use UnicaenIdref\View\Helper\IdrefLinkViewHelper;
use UnicaenIdref\View\Helper\IdrefLinkViewHelperFactory;
use UnicaenIdref\View\Helper\IdrefPopupTriggerViewHelper;
use UnicaenIdref\View\Helper\IdrefPopupTriggerViewHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'idref' => [
        /**
         * URL de l'interface publique IdRef permettant la consultation des notices d'autorités produites par les
         * établissements membres des réseaux documentaires de l'ESR (Sudoc, Calames, Star).
         * On peut construire l'URL d'accès pérenne à une notice en ajoutant le PPN, ex : "https://www.idref.fr/123456789".
         */
        'idref_url' => 'https://www.idref.fr'
    ],
    'router' => [
        'routes' => [
            'unicaen-idref' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/unicaen-idref',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
            ],
        ],
    ],
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    /**
                     * @see IndexController::indexAction()
                     */
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'roles' => 'user',
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
        ]
    ],
    'view_helpers' => [
        'factories' => [
            IdrefPopupTriggerViewHelper::class => IdrefPopupTriggerViewHelperFactory::class,
            IdrefLinkViewHelper::class => IdrefLinkViewHelperFactory::class,
        ],
        'aliases' => [
            'idrefPopupTrigger' => IdrefPopupTriggerViewHelper::class,
            'idrefLink' => IdrefLinkViewHelper::class,
        ],
        'shared' => [
            IdrefPopupTriggerViewHelper::class => false,
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    /**
     * NB : Lors d'un `composer install` fait par une appli requérant ce module, le répertoire
     * `public/unicaen` de ce module doit être copié dans le répertoire "public/" de l'appli en question grâce
     * à la "post install command" suivante :
     *
     * "scripts": {
     *      "post-install-cmd": [
     *          "mkdir -p public/unicaen ; cp -r vendor/unicaen/idref/public/unicaen public/"
     *
     * Les chemins ci-dessous sont donc relatifs au dossier racine de l'appli.
     */
    'public_files' => [
        'head_scripts' => [
            '099_unicaen-idref_js_1' => '/unicaen/idref/js/formulaire.js',
            '099_unicaen-idref_js_2' => '/unicaen/idref/js/subModal.js',
            '099_unicaen-idref_js_3' => '/unicaen/idref/js/trigger.js',
        ],
        'stylesheets' => [
            '099_unicaen-idref_css_1' => '/unicaen/idref/css/subModal.css',
            '099_unicaen-idref_css_2' => '/unicaen/idref/css/trigger.css',
        ],
    ],
];