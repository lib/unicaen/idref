<?php

namespace UnicaenIdref\View\Helper;

use Laminas\View\Helper\AbstractHelper;

/**
 * Aide de vue dessinant un lien pointant vers l'URL d'accès pérenne à une notice IDRef.
 *
 * @property \Laminas\View\Renderer\PhpRenderer $view
 */
class IdrefLinkViewHelper extends AbstractHelper
{
    private ?string $idrefUrl = null;
    private string $ppn;

    public function setIdrefUrl(string $idrefUrl): self
    {
        $this->idrefUrl = rtrim($idrefUrl, '/');

        return $this;
    }

    /**
     * Point d'entrée.
     *
     * @param string $ppn PPN de la notice
     */
    public function __invoke(string $ppn): self
    {
        $this->ppn = $ppn;

        return $this;
    }

    public function __toString(): string
    {
        if (!$this->idrefUrl) {
            return sprintf('<a href="#" onclick="return false" title="Erreur : URL d\'accès pérenne aux notices IdRef non fournie">%s</a>', $this->ppn);
        }

        return sprintf('<a href="%s">%s</a>', $this->idrefUrl . '/' . $this->ppn, $this->ppn);
    }
}