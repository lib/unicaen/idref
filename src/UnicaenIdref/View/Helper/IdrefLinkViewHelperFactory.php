<?php

namespace UnicaenIdref\View\Helper;

use Psr\Container\ContainerInterface;

class IdrefLinkViewHelperFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): IdrefLinkViewHelper
    {
        $helper = new IdrefLinkViewHelper();

        $config = $container->get('Config');
        $url = $config['idref']['idref_url'] ?? null;
        if ($url) {
            $helper->setIdrefUrl($url);
        }
        
        return $helper;
    }
}