<?php

class Constants
{
    const NOM_PERSONNE = 'Nom de personne';
    const NOM_COLLECTIVITE = 'Nom de collectivité';
    const NOM_COMMUN = 'Nom commun';
    const TITRE = 'Titre';
    const AUTEUR_TITRE = 'Auteur-Titre';
    const PPN = 'Ppn';
}