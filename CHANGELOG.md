Journal des modifications
=========================

2.1.0
-----
- Suppression de la dépendance avec laminas/laminas-dependency-plugin.

2.0.0
-----
- Requiert la nouvelle bibliothèque unicaen/privilege.

1.0.0
-----
- Première version officielle !
- Chargement systématique du js et du css pour que ça fonctionne dans une modale.
- Augmentation du z-index pour que l'iframe soit au-dessus des modales.
